class AppConstants {
  //App related
  static const String appName = 'Setup App';
  static const int appVersion = 1;

  //Languages
  static const String languageEnglish = "en";
  static const String languageGerman = "de";

  //API related
  static const String baseApi = 'http://restapi.adequateshop.com/api';
  static const String login = '$baseApi/authaccount/login';

  //API results
  static const String checkInFail = 'Check-In Failed';

  /*
Shared Pref
  */
  static const String apiKey = 'apiKey';


// login register
  static const String createAccount = "Create account";
  static const String getStarted = "Get Started";

  static const String haveAnAccount = "Have an account? ";
  static const String loginApp = "Log in";
  static const String loginNow = "Log Now";
  static const String register = "Register";
  static const String dontHaveAccount = "Don’t have Account?";
  static const String registerNow = " Register Now";
  static const String Signup = " Sign Up";
  static const String alreadyHaveAnAccount = "Already have an Account? ";

  static const String firstName = "First Name";
  static const String lastName = "Last Name";
  static const String email = "Email";
  static const String emailAddress = "Email Address";
  static const String password = "Password";
  static const String mobileNumber = "Mobile Number";
  static const String phoneNumber = "Phone Number";
  static const String gender = "Gender";
  static const String forgotPassword = "Forgot Password?";
  static const String forgotPW = "Forgot Password";
  static const String notification = "Notification";

  static const String oldPassword = "Old Password";
  static const String newPassword = "New Password";
  static const String confirmPassword = "Repeat New Password";

  static const String saveChanges = "Save Changes";
  static const String orContinueWith = "Or Sign in with Google";
  static const String continueWithGoogle = "Google";

//


}
