import 'package:flutter/material.dart';
import 'package:flutter_blocs_demo/style/dimensions.dart';

class RoundedButton extends StatelessWidget {
  String buttonName;
  Color color;

  Color textcolor;

  VoidCallback? onTap;

  RoundedButton(
      {required Color this.color,
      required this.onTap,
      required String this.buttonName,
      required this.textcolor});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: Dimensions.screenWidth,
        height: Dimensions.screenWidth * 0.12,
        //  height: 48,
        decoration: BoxDecoration(
            color: this.color,

            // border: Border.all(color: borderColor!),
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: Center(
            child: Text(
          this.buttonName,
          style: TextStyle(fontSize: 16, color: textcolor),
        )),
      ),
    );
  }
}
