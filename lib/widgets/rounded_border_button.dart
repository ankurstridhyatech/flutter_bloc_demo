import 'package:flutter/material.dart';

import '../style/dimensions.dart';

class RoundedBorderButton extends StatelessWidget {
  String buttonName;
  Color color;
  Color borderColor;
  Color textcolor;
  VoidCallback? onTap;

  RoundedBorderButton(
      {required Color this.color,
      required this.borderColor,
      required this.onTap,
      required String this.buttonName,
      required this.textcolor});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: Container(
          width: Dimensions.screenWidth,
          height: Dimensions.screenWidth * 0.12,
          //  height: 48,
          decoration: BoxDecoration(
              color: this.color,
              //   gradient: LinearGradient(
              //     colors: [Color(0xFFFF0B8D), Color(0xFFF27D2F)],
              //     begin: Alignment.centerLeft,
              //     end: Alignment.centerRight,
              //   ),

              border: Border.all(color: borderColor),
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: Center(
              child: Text(
            this.buttonName,
            style: TextStyle(fontSize: 16, color: textcolor),
          )),
        ));
  }
}
