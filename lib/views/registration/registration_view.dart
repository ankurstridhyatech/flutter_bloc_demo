/*
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blocs_demo/style/color_constants.dart';
import 'package:flutter_blocs_demo/style/dimensions.dart';
import 'package:flutter_blocs_demo/widgets/rounded_button.dart';

import 'bloc/registration_bloc.dart';

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter Bloc Registration"),
        centerTitle: true,
      ),
      body: _buildScaffoldBody(),
    );
  }

  Widget _buildScaffoldBody() {
    return BlocConsumer<RegistrationBloc, RegistrationState>(
      builder: (context, state) {
        return Container(
          // color: Colors.green,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _buildTextWidget(state),
              const SizedBox(
                height: 16,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: Dimensions.screenWidth * 0.3),
                child: RoundedButton(
                    onTap: () {
                      context.read<RegistrationBloc>().add(RegistrationButtonTappedEvent());
                    },
                    textcolor: ColorConstants.white,
                    color: ColorConstants.appColor,
                    buttonName: 'Tap me!!!'),
              ),
              */
/*   TextButton(
                onPressed: () {
                  context.read<RegistrationBloc>().add(RegistrationButtonTappedEvent());
                },
                child: const Text("Tap me!!!"),
              ),*//*

              const SizedBox(
                height: 16,
              ),
              TextButton(
                  onPressed: () {
                    context
                        .read<RegistrationBloc>()
                        .add(ShowSnackBarButtonTappedEvent());
                  },
                  child: const Text(
                    "Show Snackbar",
                    style: TextStyle(fontSize: 16),
                  ))
            ],
          ),
        );
      },
      listener: (context, state) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('This is a snack bar!!!!'),
        ));
      },
      buildWhen: (previous, current) => _shouldBuildFor(current),
      listenWhen: (previous, current) => _shouldListenFor(current),
    );
  }

  bool _shouldListenFor(RegistrationState currentState) {
    return currentState is ShowSnackbarState;
  }

  bool _shouldBuildFor(RegistrationState currentState) {
    return currentState is RegistrationInitial || currentState is UpdateTextState;
  }

*/
/*
  Widget _buildParentWidget(BuildContext context, RegistrationState state) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _buildTextWidget(state),
          const SizedBox(
            height: 16,
          ),
          TextButton(
            onPressed: () {
              context.read<RegistrationBloc>().add(RegistrationButtonTappedEvent());
            },
            child: const Text("Tap me!!!"),
          ),
          const SizedBox(
            height: 16,
          ),
          TextButton(
              onPressed: () {
                context.read<RegistrationBloc>().add(ShowSnackBarButtonTappedEvent());
              },
              child: const Text("Show Snackbar"))
        ],
      ),
    );
  }*//*


  Widget _buildTextWidget(RegistrationState state) {
    if (state is UpdateTextState) {
      return Text(state.text, style: TextStyle(fontSize: 16));
    } else {
      return const Text("Change on button tap", style: TextStyle(fontSize: 16));
    }
  }
}

*/


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blocs_demo/views/registration/bloc/registration_bloc.dart';

class RegistrationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bloc pattern'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            buildTextField(
                'UserName',
                false,
                    (val) =>
                    context.read<RegistrationBloc>().add(UserNameChanged(val))),
            buildTextField(
                'Password',
                true,
                    (val) =>
                    context.read<RegistrationBloc>().add(PasswordChanged(val))),
            buildTextField(
                'Confirm Password',
                true,
                    (val) => context
                    .read<RegistrationBloc>()
                    .add(ConfirmPasswordChanged(val))),
            BlocBuilder<RegistrationBloc, RegistrationState>(
              buildWhen: (oldState, newState) =>
              oldState.model.isValidForRegistration !=
                  newState.model.isValidForRegistration, // re-build only when isValid changed from oldstate.
              builder: (context, state) {
                return RaisedButton(
                  onPressed: state != null && state.model.isValidForRegistration
                      ? () {} // enable press call only when model is valid for registration
                      : null,
                  child: Text('Register'),
                );
              },
            )
          ],
        ),
      ),
    );
  }

  TextField buildTextField(
      String hintText, bool isObscure, ValueChanged<String> onChangedCallback) {
    return TextField(
      obscureText: isObscure,
      decoration: InputDecoration(hintText: hintText),
      onChanged: onChangedCallback,
    );
  }
}