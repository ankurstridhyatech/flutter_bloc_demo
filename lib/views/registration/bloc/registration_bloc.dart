import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_blocs_demo/views/registration/model/registration_model.dart';

import 'user_repository.dart';

part 'registration_event.dart';
part 'registration_state.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  final UserRepository _userRepository;
  RegistrationBloc(this._userRepository) : super(RegistrationInitial()) {
    // LoginBloc() : super(LoginInitial()) {
    on<RegistrationEvent>(_registrationEventHandler);

    // Will be used Later
    // on<LoginButtonTappedEvent>(_loginButtonTapped);
    // on<ShowSnackBarButtonTappedEvent>(_showSnackBarTapped);
  }

  Future<void> _registrationEventHandler(RegistrationEvent e, Emitter emit) async {
    // Perform some task run some business registration

    emit(RegistrationInitial());
  }


  @override
  RegistrationState get initialState => RegistrationInitial();

  @override
  Stream<RegistrationState> mapEventToState(
      RegistrationEvent event,
      ) async* {
    if (event is UserNameChanged) {
      bool isValidModel = await isValid(
          event.userName, state.model.password, state.model.confirmPassword);
      yield RegistrationModelChanged(state.model
          .copyWith(userName: event.userName, isValid: isValidModel));
    }
    if (event is PasswordChanged) {
      bool isValidModel = await isValid(
          state.model.userName, event.password, state.model.confirmPassword);
      yield RegistrationModelChanged(state.model
          .copyWith(password: event.password, isValid: isValidModel));
    }
    if (event is ConfirmPasswordChanged) {
      bool isValidModel = await isValid(
          state.model.userName, state.model.password, event.confirmPassword);
      yield RegistrationModelChanged(state.model.copyWith(
          confirmPassword: event.confirmPassword, isValid: isValidModel));
    }
  }

  Future<bool> isValid(
      String userName, String password, String confirmPassword) async {
    bool isValidUser = await _userRepository.isUserAvailable(userName); //username is available to create

    RegExp exp = new RegExp(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    bool isUserNameValid = userName.length >= 8; // user name should have more than or equal to 8 characters
    bool isValidPassword = exp.hasMatch(password); // password should have one small case, one upper case, one number and one symbol
    bool isConfirmPasswordMatched = password == confirmPassword; // confirm password should match with the above password

    return isValidUser &&
        isUserNameValid &&
        isValidPassword &&
        isConfirmPasswordMatched; // true if all the conditions are true, else false
  }
}

/*
import 'package:bloc/bloc.dart';

part 'registration_event.dart';

part 'registration_state.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  RegistrationBloc() : super(RegistrationInitial()) {
    on<RegistrationEvent>(_registrationEventHandler);

    // Will be used Later
    on<RegistrationButtonTappedEvent>(_registrationButtonTapped);
    on<ShowSnackBarButtonTappedEvent>(_showSnackBarTapped);
  }

  Future<void> _registrationEventHandler(RegistrationEvent e, Emitter emit) async {
    // Perform some task run some business registration

    emit(RegistrationInitial());
  }

  // Will be used Later
  Future<void> _registrationButtonTapped(
      RegistrationButtonTappedEvent e, Emitter emit) async {
    emit(UpdateTextState(text: "Text is sent from the Bloc",));
  }

  Future<void> _showSnackBarTapped(
      ShowSnackBarButtonTappedEvent e, Emitter emit) async {
    emit(ShowSnackbarState());
  }
}
*/



