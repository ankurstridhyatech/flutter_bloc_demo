import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blocs_demo/style/color_constants.dart';
import 'package:flutter_blocs_demo/style/dimensions.dart';
import 'package:flutter_blocs_demo/views/login_new/authentication/authentication.dart';
import 'package:flutter_blocs_demo/widgets/rounded_button.dart';

import 'package:flutter_blocs_demo/views/login_new/home/bloc/home_bloc.dart';
 // part   'bloc/home_bloc.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (_) => const HomeScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.appColor,
        title: const Text("Home"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Builder(
              builder: (context) {
                final userId = context.select(
                  (AuthenticationBloc bloc) => bloc.state.user.id,
                );
                return Text(
                  'UserID: $userId',
                  style: TextStyle(fontSize: 16),
                );
              },
            ),

            Builder(
              builder: (context) {
                return _buildScaffoldBody();
              },
            ),
            //
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 12.0),
            //   child: _buildScaffoldBody(),
            // ),


            Padding(
              padding: EdgeInsets.only(
                  top: 20,
                  right: Dimensions.screenWidth / 4,
                  left: Dimensions.screenWidth / 4),
              child: RoundedButton(
                  onTap: () {
                    context
                        .read<AuthenticationBloc>()
                        .add(AuthenticationLogoutRequested());
                  },
                  textcolor: ColorConstants.white,
                  color: ColorConstants.appColor,
                  buttonName: 'Logout'),
            ),
          ],
        ),
      ),
    );
  }
  Widget _buildScaffoldBody() {
    return BlocConsumer<HomeBloc, HomeState>(
      builder: (context, state) {
        return Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            const SizedBox(
              height: 16,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.screenWidth * 0.3,
              vertical: 16),
              child: RoundedButton(
                  onTap: () {
                    context.read<HomeBloc>().add(HomeButtonTappedEvent());
                  },
                  textcolor: ColorConstants.white,
                  color: ColorConstants.appColor,
                  buttonName: 'Tap me!!!'),
            ),
            _buildTextWidget(state),
            /*   TextButton(
              onPressed: () {
                context.read<HomeBloc>().add(HomeButtonTappedEvent());
              },
              child: const Text("Tap me!!!"),
            ),*/
            const SizedBox(
              height: 16,
            ),
            TextButton(
                onPressed: () {
                  context
                      .read<HomeBloc>()
                      .add(ShowSnackBarButtonTappedEvent());
                },
                child: const Text(
                  "Show Snackbar",
                  style: TextStyle(fontSize: 16),
                ))
          ],
        );
      },
      listener: (context, state) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('This is a snack bar!!!!'),
        ));
      },
      buildWhen: (previous, current) => _shouldBuildFor(current),
      listenWhen: (previous, current) => _shouldListenFor(current),
    );
  }

  bool _shouldListenFor(HomeState currentState) {
    return currentState is ShowSnackbarState;
  }

  bool _shouldBuildFor(HomeState currentState) {
    return currentState is HomeInitial || currentState is UpdateTextState;
  }


  Widget _buildTextWidget(HomeState state) {
    if (state is UpdateTextState) {
      return Text(state.text, style: TextStyle(fontSize: 16));
    } else {
      return const Text("Change on button tap", style: TextStyle(fontSize: 16));
    }
  }
}

