part of 'home_bloc.dart';

abstract class HomeState {
  const HomeState();
}

class HomeInitial extends HomeState {}

class UpdateTextState extends HomeState {
  final String text;

  UpdateTextState({required this.text});
}

class ShowSnackbarState extends HomeState {}
