import 'package:bloc/bloc.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial()) {
    on<HomeEvent>(_loginEventHandler);

    // Will be used Later
    on<HomeButtonTappedEvent>(_loginButtonTapped);
    on<ShowSnackBarButtonTappedEvent>(_showSnackBarTapped);
  }

  Future<void> _loginEventHandler(HomeEvent e, Emitter emit) async {
    // Perform some task run some business login

    emit(HomeInitial());
  }

  // Will be used Later
  Future<void> _loginButtonTapped(
      HomeButtonTappedEvent e, Emitter emit) async {
    emit(UpdateTextState(
      text: "Text is sent from the Bloc",
    ));
  }

  Future<void> _showSnackBarTapped(
      ShowSnackBarButtonTappedEvent e, Emitter emit) async {
    emit(ShowSnackbarState());
  }
}
