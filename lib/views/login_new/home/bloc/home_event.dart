part of 'home_bloc.dart';

// It's a simple abstract class
// which can be extended for other event's
abstract class HomeEvent {
  const HomeEvent();
}

// Will be used later
class HomeButtonTappedEvent extends HomeEvent {}

class ShowSnackBarButtonTappedEvent extends HomeEvent {}
