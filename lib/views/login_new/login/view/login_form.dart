import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blocs_demo/style/color_constants.dart';
import 'package:flutter_blocs_demo/style/dimensions.dart';
import 'package:flutter_blocs_demo/utils/app_constants.dart';
import 'package:flutter_blocs_demo/utils/image_paths.dart';
import 'package:flutter_blocs_demo/utils/ui_utils.dart';
import 'package:flutter_blocs_demo/views/login_new/login/bloc/login_bloc.dart';
import 'package:flutter_blocs_demo/widgets/rounded_button.dart';
import 'package:flutter_blocs_demo/widgets/text_form_field_red_box.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:formz/formz.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<NewLoginBloc, NewLoginState>(
      listener: (context, state) {
        if (state.status.isSubmissionFailure) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              const SnackBar(content: Text('Authentication Failure')),
            );
        }
      },
      child: SingleChildScrollView(
        child: Container(
          //  height: Dimensions.screenHeight,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      bottom: 30.0,
                      left: 20.0,
                      right: 20.0,
                    ),
                    child: Container(
                      //  color: Colors.green,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: Dimensions.screenHeight * 0.035),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: 80,
                                  width: 80,
                                  //  color: Colors.green,
                                  child: SvgPicture.asset(
                                    ImagePath.splashLogo,
                                    height: Dimensions.screenHeight / 4,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 10.0, bottom: 10),
                                  child: Center(
                                    child: Text(
                                      "FlutterBLoC",
                                      style: TextStyle(
                                          fontSize: Dimensions.fontSize22,
                                          color: ColorConstants.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                Container(
                                  color: ColorConstants.appColor,
                                  height: 0.5,
                                  width: Dimensions.screenWidth / 4,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.screenWidth / 22,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 30.0),
                            child: Text(
                              "Login",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                  color: ColorConstants.black),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 12.0),
                                child: Text(
                                  AppConstants.email,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: ColorConstants.black),
                                ),
                              ),
                              _UsernameInput(),
                              SizedBox(
                                height: Dimensions.screenWidth / 24,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 12.0),
                                child: Text(
                                  AppConstants.password,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: ColorConstants.black),
                                ),
                              ),
                              _PasswordInput(),
                              SizedBox(
                                height: Dimensions.screenWidth / 24,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      // Get.toNamed(Routes.forgotPassword);
                                    },
                                    child: UiUtils().widgetText(
                                        AppConstants.forgotPassword,
                                        16,
                                        ColorConstants.appColor,
                                        // AppConstants.fontMedium,
                                        FontWeight.w500),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: Dimensions.screenWidth / 15,
                              ),
                              _LoginButton()
                            ],
                          ),
                          SizedBox(
                            height: Dimensions.screenHeight / 40,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    UiUtils().widgetText(
                        AppConstants.dontHaveAccount,
                        16,
                        ColorConstants.black,
                        // AppConstants.fontMedium,
                        FontWeight.w500),
                    InkWell(
                      onTap: () {
                        //  Get.toNamed(Routes.register);
                      },
                      child: UiUtils().widgetText(
                          AppConstants.Signup,
                          16,
                          ColorConstants.appColor,
                          //  AppConstants.fontMedium,
                          FontWeight.w500),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),

      // Align(
      //   alignment: const Alignment(0, -1 / 3),
      //   child: Column(
      //     mainAxisSize: MainAxisSize.min,
      //     children: [
      //       _UsernameInput(),
      //       const Padding(padding: EdgeInsets.all(12)),
      //       _PasswordInput(),
      //       const Padding(padding: EdgeInsets.all(12)),
      //       _LoginButton(),
      //     ],
      //   ),
      // ),
    );
  }
}

class _UsernameInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewLoginBloc, NewLoginState>(
      buildWhen: (previous, current) => previous.username != current.username,
      builder: (context, state) {
        return TextFormFieldWidgetRedBox(
          key: const Key('loginForm_usernameInput_textField'),
          inputFormatters: [
            LengthLimitingTextInputFormatter(10),
          ],
          autovalidateMode: AutovalidateMode.onUserInteraction,
          enabled: true,
          // controller:
          // _onLoginController.textEmailController,
          hintText: AppConstants.email,
          inputType: TextInputType.emailAddress,
          isObscureText: false,
          labelText: AppConstants.email,
          onChanged: (username) =>
              context.read<NewLoginBloc>().add(LoginUsernameChanged(username)),
          // isError: _onLoginController.isEmailEmpty.value,
          // isBox: _onLoginController.isEmailEmpty.value,
        );

        /* TextField(
          key: const Key('loginForm_usernameInput_textField'),
          onChanged: (username) =>
              context.read<NewLoginBloc>().add(LoginUsernameChanged(username)),
          decoration: InputDecoration(
            labelText: 'username',
            errorText: state.username.invalid ? 'invalid username' : null,
          ),
        );*/
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewLoginBloc, NewLoginState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return TextFormFieldWidgetRedBox(
          // key: const Key('loginForm_passwordInput_textField'),
          inputFormatters: [
            LengthLimitingTextInputFormatter(10),
          ],
          autovalidateMode: AutovalidateMode.onUserInteraction,
          enabled: true,
          // isObscureText: true,
          // controller:
          // _onLoginController.textPasswordController,
          hintText: AppConstants.password,
          inputType: TextInputType.emailAddress,
          labelText: AppConstants.password,
          passwordButton: IconButton(
            // icon: _onLoginController
            //     .isPasswordVisible.value ==
            //     false
            //     ? SvgPicture.asset(
            //   ImagePath.pw_visible,
            // )
            //     : SvgPicture.asset(
            //   ImagePath.pw_invisible,
            // ),
            icon: SvgPicture.asset(
              ImagePath.pw_invisible,
            ),
            onPressed: () {
              // _onLoginController.isPasswordVisible.value =
              // !_onLoginController
              //     .isPasswordVisible.value;
            },
          ),
          onChanged: (password) =>
              context.read<NewLoginBloc>().add(LoginPasswordChanged(password)),

          // obscureText: true,
          // isError:
          // _onLoginController.isPasswordEmpty.value,
          // isBox: _onLoginController.isPasswordEmpty.value,
        );
        /* TextField(
          key: const Key('loginForm_passwordInput_textField'),
          onChanged: (password) =>
              context.read<NewLoginBloc>().add(LoginPasswordChanged(password)),
          obscureText: true,
          decoration: InputDecoration(
            labelText: 'password',
            errorText: state.password.invalid ? 'invalid password' : null,
          ),
        );*/
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewLoginBloc, NewLoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status.isSubmissionInProgress
            ? Center(child: const CircularProgressIndicator())
            : Padding(
                padding: EdgeInsets.only(right: 20, left: 20),
                child: RoundedButton(
                    onTap: state.status.isValidated
                        ? () {
                            context
                                .read<NewLoginBloc>()
                                .add(const LoginSubmitted());
                          }
                        : null,
                    textcolor: ColorConstants.white,
                    color: ColorConstants.appColor,
                    buttonName: 'Login'),
              );
      },
    );
  }
}
