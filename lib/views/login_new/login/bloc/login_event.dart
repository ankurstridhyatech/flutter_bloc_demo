part of 'login_bloc.dart';

abstract class NewLoginEvent extends Equatable {
  const NewLoginEvent();

  @override
  List<Object> get props => [];
}

class LoginUsernameChanged extends NewLoginEvent {
  const LoginUsernameChanged(this.username);

  final String username;

  @override
  List<Object> get props => [username];
}

class LoginPasswordChanged extends NewLoginEvent {
  const LoginPasswordChanged(this.password);

  final String password;

  @override
  List<Object> get props => [password];
}

class LoginSubmitted extends NewLoginEvent {
  const LoginSubmitted();
}
