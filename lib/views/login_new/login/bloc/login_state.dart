part of 'login_bloc.dart';

class NewLoginState extends Equatable {
  const NewLoginState({
    this.status = FormzStatus.pure,
    this.username = const Username.pure(),
    this.password = const Password.pure(),
  });

  final FormzStatus status;
  final Username username;
  final Password password;

  NewLoginState copyWith({
    FormzStatus? status,
    Username? username,
    Password? password,
  }) {
    return NewLoginState(
      status: status ?? this.status,
      username: username ?? this.username,
      password: password ?? this.password,
    );
  }

  @override
  List<Object> get props => [status, username, password];
}
