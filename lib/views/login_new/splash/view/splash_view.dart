import 'package:flutter/material.dart';
import 'package:flutter_blocs_demo/style/color_constants.dart';
import 'package:flutter_blocs_demo/style/dimensions.dart';
import 'package:flutter_blocs_demo/utils/image_paths.dart';
import 'package:flutter_blocs_demo/widgets/rounded_button.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (_) => const SplashScreen());
  }

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body:
      Container(
        color: ColorConstants.white,
        height: Dimensions.screenHeight,
        width: Dimensions.screenWidth,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [


              SvgPicture.asset(
                ImagePath.splashLogo,
                height: Dimensions.screenHeight / 4,
              ),

              SizedBox(
                height: Dimensions.screenWidth / 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 0.0),
                child: Text(
                  "Flutter BLoC",
                  style: TextStyle(
                      fontSize: Dimensions.fontSize26,
                      fontWeight: FontWeight.bold),
                ),
              ),

            ],
          ),
        ),
      ),
      // Container(
      //   color: ColorConstants.white,
      //   height: Dimensions.screenHeight,
      //   width: Dimensions.screenWidth,
      //   child: Column(
      //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //     //  mainAxisAlignment: MainAxisAlignment.center,
      //     children: [
      //       Padding(
      //         padding: const EdgeInsets.all(20.0),
      //         child: Column(
      //           crossAxisAlignment: CrossAxisAlignment.start,
      //           children: [
      //             SizedBox(
      //               height: Dimensions.screenWidth / 5,
      //             ),
      //             Padding(
      //               padding: const EdgeInsets.only(left: 8.0),
      //               child: Text(
      //                 "Flutter BLoC",
      //                 style: TextStyle(
      //                     fontSize: Dimensions.fontSize26,
      //                     fontWeight: FontWeight.bold),
      //               ),
      //             ),
      //             SizedBox(
      //               height: Dimensions.screenWidth / 15,
      //             ),
      //             Padding(
      //               padding: const EdgeInsets.only(left: 8.0),
      //               child: Text(
      //                 "Lorem ipsum is simply dummy text of the printing and typesetting industry make a type specimen book",
      //                 style: TextStyle(
      //                     color: ColorConstants.grayA5,
      //                     fontSize: Dimensions.fontSize18,
      //                     fontWeight: FontWeight.w400),
      //               ),
      //             ),
      //             SizedBox(
      //               height: Dimensions.screenWidth / 8,
      //             ),
      //             Padding(
      //               padding:
      //                   EdgeInsets.only(right: Dimensions.screenWidth / 1.7),
      //               child: RoundedButton(
      //                   onTap: () {},
      //                   textcolor: ColorConstants.white,
      //                   color: ColorConstants.appColor,
      //                   buttonName: 'Next'),
      //             ),
      //             SizedBox(
      //               height: Dimensions.screenWidth / 10,
      //             ),
      //           ],
      //         ),
      //       ),
      //       Padding(
      //         padding: EdgeInsets.only(bottom: Dimensions.screenWidth / 3),
      //         child: Container(
      //           //  color: Colors.green,
      //           child: SvgPicture.asset(
      //             ImagePath.splashLogo,
      //             height: Dimensions.screenHeight / 4,
      //           ),
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}
