// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_blocs_demo/style/color_constants.dart';
// import 'package:flutter_blocs_demo/style/dimensions.dart';
// import 'package:flutter_blocs_demo/widgets/rounded_button.dart';
//
// import 'bloc/login_bloc.dart';
//
// class LoginScreen extends StatelessWidget {
//   const LoginScreen({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     Dimensions.screenWidth = MediaQuery.of(context).size.width;
//     Dimensions.screenHeight = MediaQuery.of(context).size.height;
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text("Flutter Bloc Login"),
//         centerTitle: true,
//       ),
//       body: _buildScaffoldBody(),
//     );
//   }
//
//   Widget _buildScaffoldBody() {
//     return BlocConsumer<LoginBloc, LoginState>(
//       builder: (context, state) {
//         return Container(
//           // color: Colors.green,
//           width: double.infinity,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               _buildTextWidget(state),
//               const SizedBox(
//                 height: 16,
//               ),
//               Padding(
//                 padding: EdgeInsets.symmetric(
//                     horizontal: Dimensions.screenWidth * 0.3),
//                 child: RoundedButton(
//                     onTap: () {
//                       context.read<LoginBloc>().add(LoginButtonTappedEvent());
//                     },
//                     textcolor: ColorConstants.white,
//                     color: ColorConstants.appColor,
//                     buttonName: 'Tap me!!!'),
//               ),
//    TextButton(
//                 onPressed: () {
//                   context.read<LoginBloc>().add(LoginButtonTappedEvent());
//                 },
//                 child: const Text("Tap me!!!"),
//               ),
//
//               const SizedBox(
//                 height: 16,
//               ),
//               TextButton(
//                   onPressed: () {
//                     context
//                         .read<LoginBloc>()
//                         .add(ShowSnackBarButtonTappedEvent());
//                   },
//                   child: const Text(
//                     "Show Snackbar",
//                     style: TextStyle(fontSize: 16),
//                   ))
//             ],
//           ),
//         );
//       },
//       listener: (context, state) {
//         ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
//           content: Text('This is a snack bar!!!!'),
//         ));
//       },
//       buildWhen: (previous, current) => _shouldBuildFor(current),
//       listenWhen: (previous, current) => _shouldListenFor(current),
//     );
//   }
//
//   bool _shouldListenFor(LoginState currentState) {
//     return currentState is ShowSnackbarState;
//   }
//
//   bool _shouldBuildFor(LoginState currentState) {
//     return currentState is LoginInitial || currentState is UpdateTextState;
//   }
//
//
//   Widget _buildTextWidget(LoginState state) {
//     if (state is UpdateTextState) {
//       return Text(state.text, style: TextStyle(fontSize: 16));
//     } else {
//       return const Text("Change on button tap", style: TextStyle(fontSize: 16));
//     }
//   }
// }
