import 'package:flutter_blocs_demo/model/login_master.dart';

abstract class BaseServices {
  Future<LoginMaster?> login(
      {required String email, required String password, onNoInternet});
}
