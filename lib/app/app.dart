// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_blocs_demo/views/login/bloc/login_bloc.dart';
// import 'package:flutter_blocs_demo/views/login/login_view.dart';
// import 'package:flutter_blocs_demo/views/registration/bloc/registration_bloc.dart';
// import 'package:flutter_blocs_demo/views/registration/bloc/user_repository.dart';
// import 'package:flutter_blocs_demo/views/registration/registration_view.dart';
// import 'package:get/get.dart';
//
// import '../style/color_constants.dart';
//
// class MyApp extends StatelessWidget {
//   MyApp({Key? key}) : super(key: key);
//
//   // final AppController _controller = Get.put(AppController(), permanent: true);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Bloc Demo',
//       debugShowCheckedModeBanner: false,
//       theme: ThemeData(
//         fontFamily: "Helvetica Neue",
//         primarySwatch: ColorConstants.appColor,
//       ),
//       home: MyRegistrationScreen(),
//     );
//     /*GetMaterialApp(
//       debugShowCheckedModeBanner: false,
//       theme: ThemeData(
//         fontFamily: "Helvetica Neue",
//         primarySwatch: ColorConstants.appColor,
//       ),
//       builder: (BuildContext context, Widget? child) {
//         final MediaQueryData data = MediaQuery.of(context);
//         return MediaQuery(
//             child: child!, data: data.copyWith(textScaleFactor: 1));
//       },
//       locale: Locale(_controller.locale),
//       localizationsDelegates: const [
//         S.delegate,
//         GlobalMaterialLocalizations.delegate,
//         GlobalWidgetsLocalizations.delegate,
//         GlobalCupertinoLocalizations.delegate,
//       ],
//       supportedLocales: S.delegate.supportedLocales,
//       initialRoute: Routes.SPLASH,
//       getPages: AppPages.routes,
//       title: AppConstants.appName,
//       initialBinding: SplashBinding(),
//     );*/
//   }
// }
//
// Widget MyLoginScreen() {
//   return BlocProvider(
//     create: (BuildContext context) {
//       return LoginBloc();
//     },
//     child: const LoginScreen(),
//   );
// }
//
// Widget MyRegistrationScreen() {
//   final UserRepository userRepository = UserRepository();
//   return BlocProvider(
//     create: (BuildContext context) {
//       return RegistrationBloc(userRepository);
//     },
//     child:  RegistrationScreen(),
//   );
// }
