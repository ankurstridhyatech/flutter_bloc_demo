part of 'app_pages.dart';

abstract class Routes {
  Routes._();

  static const SPLASH = '/';
  static const WELCOME = '/welcome';
  static const LOGIN = '/login';
  static const DASHBOARD = '/dashboard';
  static const HOME = '/home';
  static const CATEGORY = '/category';
  static const PROFILE = '/profile';
  static const CART = '/cart';
  static const SCHOOLBOOKS = '/schoolbooks';
  static const CLASS1 = '/class1';
  static const DRAWER = '/drawer';
}
